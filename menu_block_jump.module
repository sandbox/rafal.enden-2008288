<?php

/**
 * Implements hook_form_FORM_ID_alter();
 */
function menu_block_jump_form_menu_block_add_block_form_alter(&$form, &$form_state, $form_id) {
  return _menu_block_jump_form($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter();
 */
function menu_block_jump_form_block_admin_configure_alter(&$form, &$form_state, $form_id) {
  return _menu_block_jump_form($form, $form_state, $form_id);
}

/**
 * Helper function for altering forms.
 */
function _menu_block_jump_form(&$form, &$form_state, $form_id) {
  if ($form['module']['#value'] == 'menu_block') {
    $form['settings']['jump'] = array(
      '#type' => 'checkbox',
      '#title' => t('Render menu as select list'),
      '#default_value' => variable_get('menu_block_' . $form['delta']['#value'] . '_jump', FALSE),
      '#weight' => 1001,
    );
    $form['settings']['jump_text'] = array(
      '#type' => 'textfield',
      '#title' => t('First option text'),
      '#description' => t('Leave empty to provide default text.'),
      '#default_value' => variable_get('menu_block_' . $form['delta']['#value'] . '_jump_text', ''),
      '#states' => array(
        'visible' => array(
          ':input[name="jump"]' => array('checked' => TRUE),
        ),
      ),
      '#weight' => 1002,
    );
    $form['settings']['jump_url'] = array(
      '#type' => 'textfield',
      '#title' => t('First option URL'),
      '#description' => t('Leave empty to use current page URL.'),
      '#default_value' => variable_get('menu_block_' . $form['delta']['#value'] . '_jump_url', ''),
      '#states' => array(
        'visible' => array(
          ':input[name="jump"]' => array('checked' => TRUE),
        ),
      ),
      '#weight' => 1003,
    );
    $form['settings']['menu-block-wrapper-close']['#weight'] = 1010;
    $form['#submit'][] = 'menu_block_jump_form_block_admin_configure_submit';
  }
}

/**
 * Submit callback for 'form_block_admin_configure'. 
 */
function menu_block_jump_form_block_admin_configure_submit(&$form, &$form_state) {
  if (empty($form['delta']['#value'])) {
    $block_ids = variable_get('menu_block_ids', array());
    $delta = max($block_ids);
  }
  else {
    $delta = $form['delta']['#value'];
  }

  variable_set('menu_block_' . $delta . '_jump', $form_state['values']['jump']);
  variable_set('menu_block_' . $delta . '_jump_text', $form_state['values']['jump_text']);
  variable_set('menu_block_' . $delta . '_jump_url', $form_state['values']['jump_url']);
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function menu_block_jump_form_menu_block_delete_alter(&$form, &$form_state, $delta = 0) {
  $form['#submit'][] = 'menu_block_jump_form_menu_block_delete_form_submit';
}

/**
 * Submit callback deletion of menu block.
 */
function menu_block_jump_form_menu_block_delete_form_submit(&$form, &$form_state) {
  $delta = $form_state['values']['delta'];
  variable_del('menu_block_' . $delta . '_jump');
  variable_del('menu_block_' . $delta . '_jump_text');
  variable_del('menu_block_' . $delta . '_jump_url');
}

/**
 * Implements hook_block_view_alter();
 */
function menu_block_jump_block_view_alter(&$data, $block) {
  if ($block->module == 'menu_block' && variable_get('menu_block_' . $block->delta . '_jump', FALSE)) {
    if (empty($data['content']['#content'])) {
      return;
    }
    
    $menu_links = array();
    foreach ($data['content']['#content'] as $menu_link) {
      if (isset($menu_link['#theme'])) {
        $menu_links[] = $menu_link;
      }
    }

    $data['content'] = array(
      '#theme' => 'menu_block_jump',
      '#jump_text' => variable_get('menu_block_' . $block->delta . '_jump_text'),
      '#jump_url' => variable_get('menu_block_' . $block->delta . '_jump_url'),
      '#menu_links' => $menu_links,
      '#contextual_links' => $data['content']['#contextual_links'],
    );
  }
}

/**
 * Implements hook_theme().
 */
function menu_block_jump_theme() {
  return array(
    'menu_block_jump' => array(
      'variables' => array(
        'jump_text' => NULL,
        'jump_url' => NULL,
        'menu_links' => array(),
      ),
    ),
//    'menu_block_select_menu_link' => array(),
//    'menu_block_select_menu_tree' => array(),
  );
}

// Themes jump menu select list
function theme_menu_block_jump($variables) {
  $menu_links = $variables['menu_links'];
  if (!empty($menu_links)) {
    foreach ($menu_links as $menu_link) {
      $options[url($menu_link['#href'])] = $menu_link['#title'];
    }
    ctools_include('jump-menu');
    $params = array(
      'default_value' => url(current_path()),
    );
    $form = drupal_get_form('ctools_jump_menu', $options, $params);
    
    $url = !empty($variables['jump_url']) ? url($variables['jump_url']) : '';
    $text = !empty($variables['jump_text']) ? $variables['jump_text'] : $form['jump']['#options'][''];
    array_shift($form['jump']['#options']);
    $form['jump']['#options'] = array($url => $text) + $form['jump']['#options'];

    return drupal_render($form);
  }
}



/**
 * Returns HTML for a wrapper for a menu sub-tree.
 *
 * @param $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @see template_preprocess_menu_tree()
 * @ingroup themeable
 */
function __theme_menu_tree($variables) {
  return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

/**
 * Returns HTML for a menu link and submenu.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: Structured array data for a menu link.
 *
 * @ingroup themeable
 */
function __theme_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

//function menu_block_jump_theme_registry_alter(&$theme_registry) {
//  //dpm($theme_registry);
//  if (!empty($theme_registry['menu_link'])) {
//    $theme_registry['menu_link']['function'] = 'menu_block_jump_menu_link';
//  }
//  if (!empty($theme_registry['menu_tree'])) {
//    $theme_registry['menu_tree']['function'] = 'menu_block_jump_menu_tree';
//  }
//}
//
//function theme_menu_block_jump_menu_link($variables) {
//  dpm('theme_menu_block_jump_menu_link');
//  return 'LINK';
//}
//
//function theme_menu_block_jump_menu_tree($variables) {
//  dpm('theme_menu_block_jump_menu_tree');
//  return 'TREE';
//}
