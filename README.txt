Requirements
------------------
1. Menu block module (http://drupal.org/project/menu_block).
2. CTools module(http://drupal.org/project/ctools).


Installation and configuration
------------------
1. Unpack and move directory "menu_block_jump" to your modules directory.
2. Enable it in the modules list of your site.
3. Edit chosen menu block and select checkbox "Render menu as select list".
4. Optionally adjust other configuration options.

Known issues
------------------
Only one level menu deeph is rendered.
